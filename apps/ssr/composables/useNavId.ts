import { TopList } from '@/config'

// 获取navId
export function useNavId() {
  const route = useRoute()
  const router = useRouter()
  const routerName = router.currentRoute.value.name
  const currentNav = TopList.find((item) => item.name === routerName)
  const queryNavId = route.query.navId ? route.query.navId : 1
  const navId = currentNav ? currentNav.value : queryNavId

  return navId
}
